# Batpad ⌨️

A handwired macropad to control my multi-screen setup.

## Hardware Features

  - 4 switches
  - 2 rotary encoders to control the screen brightness and audio volume
  - Handwired
  - Uses a Pro Micro ATmega32U4
  - Custom 3D printed case

![Batpad](doc/batpad.jpg)

## Layout 👗👔

The layout of the **Batpad** is meticulously designed to offer easy access to essential functions. For a detailed view of the keymap and its functionalities, see [the keymap for full details](qmk/keymaps/default/keymap.c).

![Base Layout](doc/keyboard-layout.png)

### Key Functions

From left to right:

- **<kbd>Rotary</kbd>**: Controls screen brightness; turn to increase or decrease brightness levels.
- **<kbd>Switch 1</kbd>**: 
  - Single tap: Sets screen brightness to minimum.
  - Double tap: Adjusts brightness to 50%.
- **<kbd>Switch 2</kbd>**: 
  - Activates the <kbd>Command</kbd> + <kbd>Space</kbd> + <kbd>mirrordisplays</kbd> command to toggle between mirror and extended desktop modes on macOS. Requires [mirror-displays](https://github.com/fcanas/mirror-displays) installed.
- **<kbd>Switch 3</kbd>**:
  - Single tap: Switches between display No.1 and No.2.
  - Double tap: Activates display No.1 on the KVM HDMI switch.
- **<kbd>Switch 4</kbd>**: Controls audio playback (play/pause).
- **<kbd>Rotary</kbd>**: Adjusts sound volume; the `RESET` button can be pressed twice quickly to flash the ATmega microcontroller.

The JSON file used for designing the layout in [keyboard-layout-editor.com](http://www.keyboard-layout-editor.com/) is available at [`doc/keyboard-layout-editor.json`](doc/keyboard-layout-editor.json).

## Electronics 📟

The Batpad is powered by an Arduino Pro Micro, which is based on the ATmega32U4 microcontroller.

### Wiring Details

- **Switches**: The columns are connected to pins `B3`, `F4`, `F6`, `F7`, and the row is connected to pin `F5`.
- **Rotary Encoders**: Wired as follows:
  - **Right Encoder**:
    - Switch: Connected to `B1` and `GND`.
    - Rotary:
      - `Rot A` to `D0`
      - `Rot B` to `C6`
      - `Common` to `GND`
  - **Left Encoder**:
    - Switch: Connected to `RST` and `GND`.
    - Rotary:
      - `Rot A` to `D1`
      - `Rot B` to `D4`
      - `Common` to `GND`

### Pinout

Refer to the pinout diagrams below for a visual representation of the connections:

- **Arduino Pro Micro Pinout**:

  ![Arduino Pro Micro](doc/arduino-pro-micro-pinout.jpg)

- **EC11 Rotary Encoder Pinout**:

  ![EC11 Rotary Pinout](doc/ec11-pinout.png)

## 3D Case 🔧

The design files, including Fusion 360, STL, and STEP formats, are available in the [case/](case) folder for those interested in customizing or replicating the case.

![Case](doc/case.png)

## Bill of Materials 🧾

To assemble your own Batpad, you will need the following components:

- [ ] 1 * ATmega32U4 microcontroller
- [ ] 4 * Cherry MX switches + keycaps
- [ ] 2 * EC11 rotary encoders
