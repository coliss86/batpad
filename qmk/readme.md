# Firmware

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Import QMK firmware and keymap

```
git clone https://gitlab.com/coliss86/batpad
git clone -b 0.17.5 https://github.com/qmk/qmk_firmware
cd qmk_firmware/keyboards
ln -s ../../batpad/qmk batpad
```

To update `qmk_firmware`
```
cd qmk_firmware
git fetch -a
git checkout <new_tag>
make git-submodule
```

## Flash the Atmega

To build the firmware, launch the following command in the root of qmk folder after setting up your build environment :

```
qmk compile -kb batpad -km default
```

To upload to your `atmega32u4`, hit twice in a second the reset switch to put the atmega in bootloader mode and launch :
```
qmk flash -kb batpad -km default
```
