/* Copyright 2020 coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "encoder.h"
#include "sendstring_french.h"
#include QMK_KEYBOARD_H

enum custom_keycodes {
    KC_MIRROR_DISPLAY = SAFE_RANGE
};

// Tap Dance declarations
enum {
    TD_BRIGHTNESS,
    TD_KVM
};

typedef enum {
  POSITION_1,
  POSITION_2
} kvm_position_state;

kvm_position_state kvm_position = POSITION_1;

void kvm(qk_tap_dance_state_t *state, void *user_data) {
  if (state->count == 1 && kvm_position == POSITION_1) {
    // switch to pos 2
    SEND_STRING( SS_TAP( X_LCTRL ) SS_DELAY(20) SS_TAP( X_LCTRL ) SS_DELAY(20) SS_DOWN( X_LSHIFT ) SS_TAP( X_2 ) SS_UP( X_LSHIFT ));
    kvm_position = POSITION_2;
    reset_tap_dance(state);
  } else if ((state->count == 1 && kvm_position == POSITION_2) || state->count >= 2) {
    // switch to pos 1
    SEND_STRING( SS_TAP( X_LCTRL ) SS_DELAY(20) SS_TAP( X_LCTRL ) SS_DELAY(20) SS_DOWN( X_LSHIFT ) SS_TAP( X_1 ) SS_UP( X_LSHIFT ));
    kvm_position = POSITION_1;
    reset_tap_dance(state);
  }
}

void brightness(qk_tap_dance_state_t *state, void *user_data) {
  if (state->count == 1) {
    SEND_STRING(SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_DOWN) SS_DELAY(20) );
    reset_tap_dance(state);
  } else if (state->count == 2) {
    SEND_STRING(SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP) SS_DELAY(20) SS_TAP(X_BRIGHTNESS_UP));
    reset_tap_dance(state);
  }
}

// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_BRIGHTNESS] = ACTION_TAP_DANCE_FN(brightness),
    [TD_KVM] = ACTION_TAP_DANCE_FN(kvm)
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT( KC_NO, TD(TD_BRIGHTNESS), KC_MIRROR_DISPLAY, TD(TD_KVM), KC_MPLY)
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case KC_MIRROR_DISPLAY:
      if (record->event.pressed) {
        SEND_STRING( SS_DOWN( X_LGUI ) SS_TAP( X_SPACE ) SS_UP( X_LGUI ) );
        _delay_ms( 100 );
        SEND_STRING("mirrordisplays"  SS_TAP( X_ENTER ) );
      }
      break;
  }
  return true;
}

bool encoder_update_user(uint8_t index, bool clockwise) {
  if (index == 0) { /* First encoder */
    if (clockwise) {
      tap_code(KC_BRIGHTNESS_UP);
      _delay_ms( 100 );
      tap_code(KC_BRIGHTNESS_UP);
      _delay_ms( 100 );
      tap_code(KC_BRIGHTNESS_UP);
    } else {
      tap_code(KC_BRIGHTNESS_DOWN);
      _delay_ms( 100 );
      tap_code(KC_BRIGHTNESS_DOWN);
      _delay_ms( 100 );
      tap_code(KC_BRIGHTNESS_DOWN);
    }
  } else {
    if (clockwise) {
      tap_code(KC_VOLU);
    } else {
      tap_code(KC_VOLD);
    }
  }
  return false;
}
